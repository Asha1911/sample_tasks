/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";
import "./App.css";
import Button from "./Button";

function App() {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    fetch("https://service.staging.recykal.com/core/locations?page=0&size=20", {
      method: "get",
      headers: {
        Authorization:
          "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxNDU4MTUiLCJhdXRoIjoiUk9MRV9GUkFOQ0hJU0VfQURNSU4sUk9MRV9GUkFOQ0hJU0VfVVNFUiIsInVzZXIiOnsidXNlcklkIjoxNDU4MTUsImZyYW5jaGlzZUlkIjo2LCJsb2dpbiI6IjE0NTgxNSIsImVtYWlsIjoiZGVtb3BhcnRuZXJAcG9rZW1haWwubmV0IiwiZXByVXNlckN1c3RvbWVySWQiOjMwNywidmVyc2lvbiI6IjEuMC4wIn0sImV4cCI6MTYzNjI2OTMyOX0.jI_MWq1I_u_Gl0TZSzoj57nbenof-05v-2ud7eN7ALNylKXyPK_8sFDYBpTchq3fQffM8GQentPesOtKE20TbQ",
        "Content-Type": "application/json;charset=UTF-8",
      },
    })
      .then((res) => res.json())
      .then((json) => setPosts(json));
  }, []);
  const handleClick = () => {
    alert("successfully clicked");
  };
  return (
    <div className="App">
      <h4>Select a place from the list</h4>
      {posts.map((post) => (
        <>
          <div className="card" onClick={() => handleClick()}>
            <img
              className="image"
              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAACjCAMAAAA3vsLfAAAAbFBMVEX///8AAADm5ubl5eXk5OTx8fH4+Pjt7e3z8/P6+vrq6ur8/Pzu7u7a2try8vIcHBzDw8NSUlJnZ2ecnJynp6e7u7vKysqzs7Otra0hISEwMDBPT0+ioqJsbGwJCQkTExOAgIBdXV2SkpJCQkIedoNQAAAJ5UlEQVR4nO2d63qcOAyGKeYMpWm7aZMettnd+7/HRdjIBoSBGRvkCfr1PfGEUd74JGNbUQQWd5b0qgJVg8pFHIsMVAOqANWCSkEVoBpQZafiKjLs4wc3Fjm3Av3PQOW9/8ngfw0q7j+YIJAYgQAaUUo00YXtwnZhCwFbAiaxgapRSWygJDZQ8mtBSWygQsGGXvf+5xIW+q8xaBWP0UhsiVHRKK4NVrkUqxz5LxPyJ7yxpdrrSUOpscr1FU3WJjFqg2LUBklsXVmyD1siHQkMW4dIUNgSAluyju2W2hYAtmZrbTsQ2yM10nVsk85Pjwaq8xvGhRTHhWI8Lgz9agDYmonXejTrxwU1iC6NCyM0dWdR1VlcR6DiTpWgclAZqBRUAaoF1YIqQDWgMlA5KAJb/y1jq/7B0n/jefE3j9ik1+nY6xJU1XOIgQOoCFQcEWh6II0EbFZHYVbHZHHeRvWrBDbK9y9Y+pUojb1i0/4nQ/9Sa/9X5m04UHqf7lK+27GJg7CxjhIo33nUNtZRAuX7mbVtPUoQqKxRAnR5FfSHda9KUDmoDFQDqgCVoipApaAyUI1SBTrHtra1KfrfoP85qHKCYaJyRNNINOa8TdBRQrI8b0vilSiBcv682tZor4koQUzbp22g9BslUM6fh83hdNdvlEA5f3ptc4FNdDZ0fp1So0GnVOfXKRUldEpFCZ1S1b1TQ78aQG1rtNeqkXZKYeuUaqQIJEkGVc3QlJ2p0QCUHA1AydEAfpii6nvTFlUKSo0LJRklUM6fh63WXqtxYfA/B1WiWgbSq1RNHIRYixKSaZRA9KuDscXWNZTB66UoYTYuUAMlw+nuFSU8PDbdDU46PxUlDKNBOx4NJv3qYIyxodfmaIb+AwaByjZQRjlY1lnZjFUBKh1U1qIqQKXlSGVdGTrHFluREv43yn9T5aBKCo0CYk5ABHIVyFXgBESYExCBVS7EKEEIgQ1F4ARETCcgGk01RnNFCVeUsG5+ogRBdX7uo4TfWPqLKK0OaaRUlDBrpLaBEjq6rAHrVQqqGKsWVQGqRZWaqkTvDGwvf83s5T8s/U0Uf/WIrbT5P8JAKRONfB7V+enRoMHRYDwBmfarBLa7zDk2OZrhBETgBERYJiDkQCkf5226yw1bIFHCY2MjG6mLKIEbtoUoYVsjNaOEFKwAo1RLqL64namMP7Zs7rWpljHMgGyPEhYmIMI6AeGFjZqAnB0lBLAHJJgogRc231GCbqR3RgkssW1tpImlkRaOrCWHhM+EvWLpD6L0p0dsakhwYPJ5905Adr1LsMekjUdsd05AjDYoH3ctioPiEyVQvj8OtpVQ3m0jZfMu4Z5Qvu/hdgUIWo2m2uTCEeX7edhKFwFCr05Ypjx9dfeGZcoxmmtR/PQoIYDa5jC4chPKv7sXfvbXCDteKITwennxNcLKC4UZEOUr8Y7GyTIl5TybCci2zQz0MqUsu6a70RUlLNqxUcLtL/wo309upG5e+GULe2syvdkoHW82avW2HVMFsQl15vWw2QhVrrcdTYCYaNhtefZ5nGPvlmfLZoYrSgAVfJTgs7adESXAB69tgQMQ6Of6HfhlXnUK9t2XDaoUVH8koUVVgEpRNepwwuYowb4onnrEVqRj/zPD/07lPYYKgMjDCcto5PMWz3vcGyW8fprZ699Y+oco/mT95Rvsw0eD22qUkJBbnjWaY6IEDmZiCyRK4GD+sBljhth4xcC2w0McbIrNyeEh+8HlbHxwue2PqhFHmOEIcAjY6IPXq0eYZ2jcXZ/ifjODI9PYHF6fwnjrjCObYeMfJXCw47Bh57eCbfWKAQ5mwTa+iGzPFQPyDh99mw8o6PyG23zmFx2NLwqq4KKjuozjlj+2NJ1fz1Tl0v/heqN6er0RqtJAI83b9Snfnmb27TOWvhHF36m//DvxmDcs/Ux9C4XN4fUpun0utGKPi+Irh4e0VcQH9XGZL9S30NgCiRLs2FZWQLSJB8Tm7YqBY7BNBtG9F5EJxKbu/IzXsMXN4ICfG7V8Y4vV3Alrm1jAJqa1LfZ8yeJ+bD5rG8cowf2iuHNsHM8luF8U91XbHhFbILXt6tuU//v6tskHOF2y6BzbLVexC35RAot522NECQfO2x4JW0i1jVHf5re2Oevb7lqmNP5lHrbOOMdmz5cgLBMQsWnedgu2EOdtXNNM7Mf23qKEEBtpmFHC0au7rqOEOO5zKfRc+zQTQCweXjaoty+dqTQTUTS8xwAvbGkmuL1LkG+PtNfATqaZAHaoVJoJBDJB4z3NBAej522s00xwsACjBA4WYJTAwby8J/WbZoKDaWw3p5nQaI5JM8HB5hOQheCKT5oJDsb7Fcz72t/m98wVB5s30rUzVxglLBw7WE0zobIqqDQTsjdta0wzgWkaltJM3HUuQRvxsdc/WPo39S0Utlp7vZhmouKaZmLz1VDaGuKDv7D0N/UtFLb3kWZCG9NF8QvbTdhsB07Pu5nBA7YbbmYgB8rlNBNGVoWyUy0qTNNQek4z4RxbMfH65jQTJec0E86xvY80E86xBRMl8MIWdJqJ07HdcH/bbKD0m2aCF7Zg0kzwwhZMmglu2K4ogQE2spG+oyiBW5oJXtjINBNb7o49Os0EL2wco4QAprtXlMAyStCN9OGjhL1pJhQQ2dGBTVSxU20eEs5bFM8W/7rpH78GRD5vJUpYnYAsRgl3ZefQ9pP44A8sfaW+hcIWTJoJDhZglMDBDo4SXDRSDrbYSB2mmaDCgjvSTHAwY+GoXUozsSVoODDNBAdzsUw5m4BcW2cktrOihABqWzBpJjiYl1Deb5oJDmZGCYGkmdD2a57U28j+/Ub9CmVvRHJwbb+oX7lzMwO9TCnL/E93s8hmT1uxPVkfk61hCy5K
  aCObPW/F9mx9THssNm8v/Lhhc/bCDzu/IZfCoFJQxVgVoFpUqalWLpDlgK2
  eew1bjDrVDKrRKqfQFIekmeCFLZg0EyyxhRQlcMB2UpqJ/dsCeWHzk2Yix1wKuas0E
  7ywUWkmckwzkRtpJjQQGo18nrc0E7ywBZhmgge2K0pggG398NDdaSb4YGOUZqJZTTP
  BAdvSwWvGaSY4YON4gewVJVxRwm3YsPNzl2aCAzZ7moldVwwsp5kABeNCDKNBlFZmmokKEjbUozQTK1GCLvaIrbBj02kmp
  Nc3p5lQw5+L61P6kaK/gIRc4f6YWCx+2YrtJbY9h/yHPYmhuNBes0kzQV6FxcueS35RwuZ2dp6dj23er4aK7d40E1OkMWKLTWwx1rZxmolQsMWIzZJmApRYRNO4SzNRBdC3fS/GXjOIEqoAapvCxilKCKG2PbPDlryz2nb1bRv6topAY/vAjighgNr2XK5HCVNi9FXs/wPWPcjVzrP8PAAAAABJRU5ErkJggg=="
            />

            <h1>{post.name}</h1>
            <div>{post?.addressInfo?.street}</div>
            <span>{post?.addressInfo?.zipcode}</span>
          </div>
        </>
      ))}
      <Button/>
    </div>
  );
}
export default App;
